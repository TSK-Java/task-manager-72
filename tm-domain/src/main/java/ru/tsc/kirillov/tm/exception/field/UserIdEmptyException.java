package ru.tsc.kirillov.tm.exception.field;

public final class UserIdEmptyException extends IdEmptyException {

    public UserIdEmptyException() {
        super("Ошибка! ID пользователя не задан.");
    }

}
