package ru.tsc.kirillov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataYamlFasterXmlLoadRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class DataLoadYamlFasterXmlListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-load-yaml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения из yaml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataLoadYamlFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Загрузка состояния приложения из yaml файла (FasterXML API)]");
        getDomainEndpoint().loadDataYamlFasterXml(new DataYamlFasterXmlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
