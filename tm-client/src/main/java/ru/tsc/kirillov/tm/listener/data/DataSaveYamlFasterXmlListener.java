package ru.tsc.kirillov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataYamlFasterXmlSaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class DataSaveYamlFasterXmlListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-save-yaml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из yaml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataSaveYamlFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Сохранение состояния приложения из yaml файла (FasterXML API)]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlFasterXmlSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
