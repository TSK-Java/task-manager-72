package ru.tsc.kirillov.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners(AuditingEntityListener.class)
public class UserDto extends AbstractAuditDtoModel {

    private static final long serialVersionUID = 2;

    @Nullable
    @Column(nullable = false, unique = true)
    private String login;

    @Nullable
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(nullable = false, unique = true)
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @JsonIgnore
    @XmlTransient
    @Column(nullable = false)
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RoleDto> roles = new ArrayList<>();

    @JsonIgnore
    @XmlTransient
    public List<RoleDto> getRoles() {
        return roles;
    }

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

    public UserDto(@Nullable final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDto(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @Nullable final String email
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public UserDto(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @NotNull final List<RoleDto> roles
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.roles = roles;
    }

}
