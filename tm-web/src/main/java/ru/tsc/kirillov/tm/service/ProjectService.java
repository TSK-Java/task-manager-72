package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.exception.AccessDeniedException;
import ru.tsc.kirillov.tm.exception.entity.EntityEmptyException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.exception.field.NameEmptyException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final ProjectDto project = new ProjectDto(
                userId,
                "Новый проект: " + System.currentTimeMillis(),
                "Описание",
                new Date(),
                new Date());
        save(userId, project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDto project = new ProjectDto(
                userId,
                name,
                "Описание",
                new Date(),
                new Date());
        save(userId, project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto save(@Nullable final String userId, @Nullable final ProjectDto project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (project == null) throw new EntityEmptyException();
        project.setUserId(userId);
        return repository.save(project);
    }

    @Nullable
    @Override
    public Collection<ProjectDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectDto findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final ProjectDto project) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (project == null) throw new EntityEmptyException();
        removeById(userId, project.getId());
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final List<ProjectDto> projects) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (projects == null) throw new EntityEmptyException();
        projects
                .stream()
                .forEach(project -> remove(userId, project));
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        repository.deleteByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return repository.countByUserId(userId);
    }
    
}
