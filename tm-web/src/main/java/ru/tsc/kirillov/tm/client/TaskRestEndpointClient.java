package ru.tsc.kirillov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.Arrays;
import java.util.List;

public class TaskRestEndpointClient implements ITaskEndpoint {

    @NotNull
    private static final String ROOT_URL = "http://localhost:8080/api/task/";

    @Override
    public List<TaskDto> findAll() {
        @NotNull final String localUrl = "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        return Arrays.asList(template.getForObject(ROOT_URL + localUrl, TaskDto[].class));
    }

    @Override
    public TaskDto findById(@NotNull final String id) {
        @NotNull final String localUrl = "findById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, TaskDto.class, id);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @NotNull final String localUrl = "existsById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Boolean.class, id);
    }

    @Override
    public TaskDto save(@NotNull final TaskDto task) {
        @NotNull final String localUrl = "save";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        return template.postForObject(ROOT_URL + localUrl, entity, TaskDto.class);

    }

    @Override
    public void delete(@NotNull final TaskDto task) {
        @NotNull final String localUrl = "delete";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity entity = new HttpEntity(task, headers);
        template.postForObject(ROOT_URL + localUrl, entity, TaskDto.class);
    }

    @Override
    public void deleteAll(@NotNull final List<TaskDto> tasks) {
        @NotNull final String localUrl = "deleteAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<TaskDto>> entity = new HttpEntity<>(tasks, headers);
        template.postForObject(ROOT_URL + localUrl, entity, TaskDto[].class);
    }

    @Override
    public void clear() {
        @NotNull final String localUrl = "clear";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl);
    }

    @Override
    public void deleteById(@NotNull final String id) {
        @NotNull final String localUrl = "deleteById/{id}";
        @NotNull final RestTemplate template = new RestTemplate();
        template.delete(ROOT_URL + localUrl, id);
    }

    @Override
    public long count() {
        @NotNull final String localUrl = "count";
        @NotNull final RestTemplate template = new RestTemplate();
        return template.getForObject(ROOT_URL + localUrl, Long.class);
    }

    public static void main(String[] args) {
        TaskRestEndpointClient client = new TaskRestEndpointClient();

        @NotNull final List<TaskDto> tasks = client.findAll();
        System.out.println("Список проектов:");
        for(@NotNull final TaskDto task: tasks) {
            System.out.printf("Проект: %s; ID: %s%n", task.getName(), task.getId());
        }
        System.out.println();

        @NotNull final String id = tasks.get(0).getId();

        System.out.printf("Количество проектов: %d%n", client.count());
        System.out.printf("Существование проекта: %b%n", client.existsById(id));

        @Nullable final TaskDto task = client.findById(id);
        System.out.printf("Поиск проекта: %s%n", task.getName());

        client.deleteById(id);
        System.out.printf("Количество проектов: %d%n", client.count());
        System.out.printf("Существование проекта: %b%n", client.existsById(id));

        client.save(task);
        System.out.printf("Количество проектов: %d%n", client.count());

        client.delete(task);
        System.out.printf("Количество проектов: %d%n", client.count());

        client.save(task);
        client.deleteAll(tasks);
        System.out.printf("Количество проектов: %d%n", client.count());

        client.save(task);
        System.out.printf("Количество проектов: %d%n", client.count());
        client.clear();
        System.out.printf("Количество проектов: %d%n", client.count());
    }

}
