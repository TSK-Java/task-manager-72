package ru.tsc.kirillov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.kirillov.tm.config.DatabaseConfiguration;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;
import ru.tsc.kirillov.tm.util.UserUtil;

import java.util.List;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskDto Task = new TaskDto("id", "Name");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Task.setUserId(UserUtil.getUserId());
        repository.save(Task);
    }

    @After
    public void clean() {
        repository.deleteAll();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        @NotNull final List<TaskDto> TaskList = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(1, TaskList.size());
    }

    @Test
    public void findFirstByUserIdAndId() {
        @Nullable final TaskDto TaskFind =
                repository.findFirstByUserIdAndId(UserUtil.getUserId(), Task.getId());
        Assert.assertNotNull(TaskFind);
        Assert.assertEquals(TaskFind.getId(), Task.getId());
    }

    @Test
    public void deleteByUserId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteByUserIdAndId(UserUtil.getUserId(), Task.getId());
        Assert.assertNull(repository.findFirstByUserIdAndId(UserUtil.getUserId(), Task.getId()));
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

}
